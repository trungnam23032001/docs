# RKE2

## Introduction

RKE2, also known as RKE Government, is Rancher's next-generation Kubernetes distribution.

It is a fully [conformant Kubernetes distribution](https://landscape.cncf.io/card-mode?selected=rke-government) that focuses on security and compliance within the U.S. Federal Government sector.

## Architecture

![Architecture](https://docs.rke2.io/assets/images/overview-06f8a098e271952bfe5db78b3a0e9b25.png)

For more information, check [here](https://docs.rke2.io/architecture)

## Installation

Before starting installation, check the [prerequisites](https://docs.rke2.io/install/quickstart#prerequisites).

You can install according to the [Quick Start guide](https://docs.rke2.io/install/quickstart). In this doc, I will build a K8s cluster with this architecture:

```yaml
servers:
  - nodename: master1
    os: "Ubuntu 20.04.4 LTS"
    resources:
      cpu: 2
      memory: 3GB
      disk: 30GB
    ip: 192.168.58.21

workers:
  - nodename: worker1
    os: "Ubuntu 20.04.4 LTS"
    resources:
      cpu: 2
      memory: 3GB
      disk: 30GB
    ip: 192.168.58.26

  - nodename: worker2
    os: "Ubuntu 20.04.4 LTS"
    resources:
      cpu: 2
      memory: 3GB
      disk: 30GB
    ip: 192.168.58.27
```

I used [Vagrant](https://www.vagrantup.com/) to create VMs in Virtuabox, you can check Vagrantfile [here](./ref/Vagrantfile)

### Server Node Installation

SSH to `master1` node and run as root user

```sh
sudo su
```

#### 1. Run the installer

```sh
curl -sfL https://get.rke2.io | sh -
```

This will install the `rke2-server` service and the `rke2` binary onto your machine

#### 2. Enable the `rke2-server` service

```sh
systemctl enable rke2-server.service
```

#### 3. Config server node

Before starting `rke2-server` service to build K8s cluster, we might want to change some parameters. Here are the options that we can use to configure rke2 server node, [Server Configuration Reference](https://docs.rke2.io/reference/server_config). This is a reference to all parameters that can be used to configure the rke2 server. Note that while this is a reference to the command line arguments, the best way to configure RKE2 is using the [configuration file](https://docs.rke2.io/install/configuration#configuration-file).

I made my configuration file like this

```sh
mkdir -p /etc/rancher/rke2/
vim /etc/rancher/rke2/config.yaml
```

with content

```yaml
# /etc/rancher/rke2/config.yaml
node-ip: 192.168.58.21
```

But it's not done yet. Due to my method of creating VMs, there was an error related to the CNI plugin (canal). Check this in [[Unable to communicate between pods on different nodes]](#unable-to-communicate-between-pods-on-different-nodes) section.

#### 4. Start the service

```sh
systemctl start rke2-server.service
```

and follow the logs, if you like

```sh
journalctl -u rke2-server -f
```

After running this installation:

- The `rke2-server` service will be installed. The `rke2-server` service will be configured to automatically restart after node reboots or if the process crashes or is killed.
- Additional utilities will be installed at `/var/lib/rancher/rke2/bin/`. They include: `kubectl`, `crictl`, and `ctr`. Note that these are not on your path by default.
- Two cleanup scripts, `rke2-killall.sh` and `rke2-uninstall.sh`, will be installed to the path at:
  - `/usr/local/bin` for regular file systems
  - `/opt/rke2/bin` for read-only and brtfs file systems
  - `INSTALL_RKE2_TAR_PREFIX/bin` if `INSTALL_RKE2_TAR_PREFIX` is set
- A [kubeconfig](https://kubernetes.io/docs/concepts/configuration/organize-cluster-access-kubeconfig/) file will be written to `/etc/rancher/rke2/rke2.yaml`.
- A token that can be used to register other server or agent nodes will be created at `/var/lib/rancher/rke2/server/node-token`

> Note:  
> If you are adding additional server nodes, you must have an odd number in total. An odd number is needed to maintain quorum. See the [High Availability documentation](https://docs.rke2.io/install/ha) for more details.

### Linux Agent (Worker) Node Installation

The steps on this section requires root level access or `sudo` to work.

#### 1. Run the installer for agent node

```sh
curl -sfL https://get.rke2.io | INSTALL_RKE2_TYPE="agent" sh -
```

This will install the `rke2-agent` service and the `rke2` binary onto your machine. Due to its nature, It will fail unless it runs as the root user or through `sudo`.

#### 2. Enable the rke2-agent service

```sh
systemctl enable rke2-agent.service
```

#### 3. Configure the rke2-agent service

```sh
mkdir -p /etc/rancher/rke2/
vim /etc/rancher/rke2/config.yaml
```

Content for config.yaml:

```yaml
# /etc/rancher/rke2/config.yaml
server: https://<server>:9345
token: <token from server node>
node-ip: <ip of this node>
```

For example, config in `worker1` is

```yaml
# /etc/rancher/rke2/config.yaml
server: https://192.168.58.21:9345
token: K101d190f6182a502650a517dfd216599b60265c0bc1c05c2400b9f43987e320087::server:9ceb5d1bfb6e0ed5faf8393a498c70b2
node-ip: 192.168.58.26
```

> Note:  
> The `rke2 server` process listens on port `9345` for new nodes to register. The Kubernetes API is still served on port `6443`, as normal.

#### 4. Start the `rke2-agent` service

```sh
systemctl start rke2-agent.service
```

follow the logs, if you like

```sh
journalctl -u rke2-agent -f
```

**Note:** Each machine must have a unique hostname. If your machines do not have unique hostnames, set the `node-name` parameter in the `config.yaml` file and provide a value with a valid and unique hostname for each node.

To read more about the config.yaml file, see the [Install Options documentation.](configuration.md#configuration-file)

## RKE2 HA

To install RKE2 in HA mode, you can follow these guide:

- [RKE2 High Availability documentation](https://docs.rke2.io/install/ha)
- [Setting up a High-availability RKE2 Kubernetes Cluster for Rancher](https://ranchermanager.docs.rancher.com/how-to-guides/new-user-guides/kubernetes-cluster-setup/rke2-for-rancher)

## Troubleshooting

### Unable to communicate between pods on different nodes

There are many reasons why pod cannot communicate to others which on different nodes. In my case, it was cause by CNI plugin (canal) and VMs creation.

Check my VM ip config

```sh
# vagrant@master1:~$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:a2:6b:fd brd ff:ff:ff:ff:ff:ff
    inet 10.0.2.15/24 brd 10.0.2.255 scope global dynamic eth0
       valid_lft 82868sec preferred_lft 82868sec
    inet6 fe80::a00:27ff:fea2:6bfd/64 scope link 
       valid_lft forever preferred_lft forever
3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:8c:5b:27 brd ff:ff:ff:ff:ff:ff
    inet 192.168.58.21/24 brd 192.168.58.255 scope global eth1
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe8c:5b27/64 scope link 
       valid_lft forever preferred_lft forever
```

Those VMs only communicate with each other by interface `eth1`. When we deploy Canal with default config, its interface is chosen using the node's default route [[check here]](https://github.com/rancher/rke2-charts/blob/main-source/packages/rke2-canal/charts/values.yaml#L12). And default route in VM is `eth0`

```consle
# vagrant@master1:~$ ip route
default via 10.0.2.2 dev eth0 proto dhcp src 10.0.2.15 metric 100 
10.0.2.0/24 dev eth0 proto kernel scope link src 10.0.2.15 
10.0.2.2 dev eth0 proto dhcp scope link src 10.0.2.15 metric 100 
192.168.58.0/24 dev eth1 proto kernel scope link src 192.168.58.21 
```

This why **pod cannot communicate to others which on different nodes**

To fix it, ovveride Canal config to choose `eth1` by creating this file

```yaml
# /var/lib/rancher/rke2/server/manifests/rke2-canal-config.yaml
---
apiVersion: helm.cattle.io/v1
kind: HelmChartConfig
metadata:
  name: rke2-canal
  namespace: kube-system
spec:
  valuesContent: |-
    flannel:
      iface: "eth1"
```

After that, restart the canal daemonset to use the newer config by executing if you deployed

```sh
kubectl rollout restart ds rke2-canal -n kube-system
```

### Plugin `calico` failed to get ClusterInformation: Unauthorized

Error message:

```txt
plugin type=\"calico\" failed (delete): error getting ClusterInformation: connection is unauthorized: Unauthorized"
```

The error causes "Creating pod sanbox" process to get stuck.  
To fix it, restart `rke2-canal` DaemonSet

```sh
kubectl rollout restart ds/rke2-canal -n kube-system
```

Ref:

- [Github#3425#issuecomment-1269512423](https://github.com/rancher/rke2/issues/3425#issuecomment-1269512423)
- [Github#3425#issuecomment-1271891935](https://github.com/rancher/rke2/issues/3425#issuecomment-1271891935)

## RKE1 vs RKE2

RKE2 comes with new installation method, architecture, and some feature such as using new CNI plugin Canal (Calico + Fannel), more security, etc.  
You can get more information about this via:

- [Rancher Official Docs](https://ranchermanager.docs.rancher.com/v2.6/how-to-guides/new-user-guides/launch-kubernetes-with-rancher/rke1-vs-rke2-differences)
- [Blog: Comparing between RKE1 vs RKE2 - Medium](https://medium.com/btech-engineering/comparing-between-rke1-vs-rke2-d945ff5aa2b2)
- [Blog: RKE vs. RKE2: Comparing Kubernetes Distros - SUSE](https://www.suse.com/c/rancher_blog/rke-vs-rke2-comparing-kubernetes-distros/)
